#!/usr/bin/env python3
import re
f = open("input.txt", "r")
val = [re.sub("[\n]",'', i) for i in f]

rules = []

mybag = 'shiny gold'

for x in val:
    print(x)
    bag, rule = re.findall(r"(.+) bag.+contain (.+)", x)[0]
    print(rule)
    
    if rule == None:
        print("SOMETHING SCREWED UP")
        exit(1)

    contains = re.findall(r"(?:[^,]*(\d)+ (.[^,.]+) bag)+", rule)

    print(bag)
    print(contains)

    rules += [[bag, contains]]
    print(rules[0])

    print("======")

foundbags = []
nonrepeatfound = []

def recursiveCheck(bagToSearch, foundbags):
    total = 0
    for rule in rules:
        bag = rule[0]
        contain = rule[1]
        for num, contbag in contain:
            if contbag == bagToSearch:
                foundbags += [bag]
                total += recursiveCheck(bag, foundbags)
                print("Bag: " + bag)
                print("Rules: " + str(rule))
                print("Contains: " + contbag)
                print("Total: " + str(total))
                print("------")
                total += 1
    return total

recursiveCheck(mybag, foundbags)

for i in foundbags:
    if i not in nonrepeatfound:
        nonrepeatfound += [i]

print(foundbags)
print(nonrepeatfound)
print(len(nonrepeatfound))
print("#######################")

######################################################
def recursiveCheckTOTAL(bagToSearch):
    total = 1
    for rule in rules:
        bag = rule[0]
        contain = rule[1]
        if bag == bagToSearch:
            print("Bag contains: " + str(contain))
            for num, contbag in contain:
                hmm = recursiveCheckTOTAL(contbag)
                total += int(num) * hmm
                print(hmm)
                print("Bag: " + bag)
                print("Rules: " + str(rule))
                print("Contains: " + contbag)
                print("Num of Bags: " + num)
                print("Total: " + str(total))
                print("------")
    return total

print(recursiveCheckTOTAL(mybag)-1)