#!/usr/bin/env python3
import re

f = open("input.txt", "r")
out = open("out.txt", "w")
val = [re.sub('\n','',i) for i in f]

xslope= [1,3,5,7,1]
yslope= [1,1,1,1,2]

# xslope= [3]
# yslope= [1]

#Part 1 and 2
def traverseHill(xslope,yslope,val):
    x1 = 0
    count = 0
    runs = 0
    for y in range(yslope,len(val),yslope):
        x1 += xslope
        x=x1%len(val[y])

        if val[y][x] == '#':
            count += 1
        runs += 1
    print(runs)
    return count


valOut = 1
lol = []
for i in range(len(xslope)):
    valOut *= traverseHill(xslope[i],yslope[i],val)
    lol.append(traverseHill(xslope[i],yslope[i],val))

print(valOut)
print(lol)