#!/usr/bin/env python3
f = open("input.txt", "r")
val = [int(i) for i in f]

print("=========")

found = False
for j in range(len(val)):
    for k in range(len(val)):
        if val[j] + val[k]== 2020:
            print("Found the two points!")
            print(val[j])
            print(val[k])
            print(val[j] * val[k])
            found = True
            break
        if found == True:
            break
    if found == True:
        break

print("=========")

found = False
for i in range(len(val)):
    for j in range(len(val)):
        for k in range(len(val)):
            if val[i] + val[j] + val[k]== 2020:
                print("Found the three points!")
                print(val[i])
                print(val[j])
                print(val[k])
                print(val[i] * val[j] * val[k])
                found = True
                break
        if found == True:
            break
    if found == True:
        break

print("=========")

if found == False:
    print("Not found")
