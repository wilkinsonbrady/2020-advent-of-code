#!/usr/bin/env python3
import re

f = open("input.txt", "r")
# val = [re.sub('\n','',i) for i in f]
val = [i for i in f]
fields = ["byr","iyr","eyr","hgt","hcl","ecl","pid"]
optfield = "cid"
entry =[]
allval = []
numofentry = 0
for i in val:
    if i != '\n':
        v = re.split("[: ]", i)
        for j in v:
            entry.append(j)
    else:
        numofentry += 1
        allval.append(entry)
        entry =[]

inputs = [[]] * len(fields)

count = 0
currval = 0
for i in allval:
    found = True
    missingfield = ''
    iss = 0
    ex = 99999999999
    for j in fields:
        if j in i and len(i)%2==0 and len(i) >= 14:
            loc = i.index(j)
            l = j.index(j)
            if not (i[loc+1] in inputs[l]):
                inputs[l].append(i[loc+1])

            if j == 'byr':
                found=(1920<=int(re.sub('\n','',i[loc+1]))<=2002)
            if j == 'iyr':
                found=(2010<=int(re.sub('\n','',i[loc+1]))<=2020)
                iss = int(re.sub('\n','',i[loc+1]))
                print(iss)
                print(found)
            if j == 'eyr':
                found=(2010<= int(re.sub('\n','',i[loc+1]))<=2030)
                ex = int(re.sub('\n','',i[loc+1]))
                print(ex)
                print(found)
            if j == 'hgt':
                found=(i[loc+1])
            if j == 'hcl':
                found=(i[loc+1])
            if j == 'ecl':
                found=(i[loc+1])
            if j == 'pid':
                found=(re.search(,i[loc+1]))

            # if loc %2 != 0:
            #     found = False

            if found == False:
                missingfield = j
                break
        else:
            missingfield = j
            found = False
            break
    if found:
        print("*****************  " +str(currval) +'     '+ str(len(i)) +'      '+str(i))
        count += 1
    else:
        print("------")
        print(str(currval)+'    '+ str(len(i))+'   ' + missingfield + '    ' + str(i))
        print("------")
    currval+=1

# for i in range(len(fields)):
#     print(fields[i])
#     print(inputs[i])
#     print("-------")



print(allval)
print(numofentry)
print(count)