#!/usr/bin/env python3
import re

f = open("input.txt", "r")
val = [re.split('[- ]', i) for i in f]

#Part 1
count = 0
for entry in val:
    numchar = len(re.sub('\n', '', entry[3]))-len(re.sub(entry[2][0], '', re.sub('\n', '', entry[3])))
    if numchar >= int(entry[0]) and numchar<=int(entry[1]):
        count+=1
print(len(val))
print(count)

print("=====")

#Part 2
count = 0
for entry in val:
    pos1 = int(entry[0])-1
    pos2 = int(entry[1])-1
    character = entry[2][0]
    password = re.sub('\n', '', entry[3])

    if (password[pos1] == character) and (password[pos2] != character) or (password[pos1] != character) and (password[pos2] == character):
        count += 1
print(len(val))
print(count)