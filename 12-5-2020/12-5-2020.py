#!/usr/bin/env python3
import math
import re
f = open("input.txt", "r")
val = [re.sub("[\n]",'', i) for i in f]

numRows=128
numColumns=8
r = int(math.log2(numRows))

rv = ''
cv = ''

seat = []
idval = []

posid= []
for i in range(numRows):
    for j in range(numColumns):
        z = i*8+j
        posid+=[z]

for i in val:
    for j in range(len(i)):
        if j < r:
            if i[j] == "F":
                rv += '0'
            else:
                rv += '1'
        else:
            if i[j] == "R":
                cv += '1'
            else:
                cv += '0'
    x = int(rv,2)
    y = int(cv,2)
    idvalue = x*8 + y
    idval += [idvalue]
    seat += [[x,y]]
    rv = ''
    cv = ''
idval.sort()
print(seat)
print("++++++++++")
print(idval)
print("++++++++++")
print(posid)
for i in posid:
    if not (i in idval):
        print(i)

print(max(idval))