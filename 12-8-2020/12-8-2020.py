#!/usr/bin/env python3
import re
f = open("input.txt", "r")
val = [re.sub("[\n]",'', i) for i in f]


accumulator = 0
executedinstructions = []
linenum = 0

while True:
    line = val[linenum]
    if linenum in executedinstructions:
        break
    else:
        executedinstructions += [linenum]

    instruction, argument = re.findall(r"(.+) [+]?(.+)*", line)[0]

    if instruction == "acc":
        accumulator+= int(argument)
    if instruction == "jmp":
        linenum += int(argument)
    else:
        linenum+=1
        
print("Part 1 answer:")
print(accumulator)


for i in range(len(val)):
    test = True
    valcopy = val[:]
    changedline = val[i]
    instruction, argument = re.findall(r"(.+) (.+)*", val[i])[0]
    if instruction == "jmp":
        valcopy[i] = "nop " + argument
    elif instruction == "nop":
        valcopy[i] = "jmp " + argument
    else:
        test = False
    


    accumulator = 0
    executedinstructions = []
    linenum = 0
    repeat = False

    while test and linenum < len(valcopy):
        line = valcopy[linenum]
        if linenum in executedinstructions:
            repeat  = True
            break
        else:
            executedinstructions += [linenum]
        instruction, argument = re.findall(r"(.+) [+]?(.+)*", line)[0]

        if instruction == "acc":
            accumulator+= int(argument)
        if instruction == "jmp":
            linenum += int(argument)
        else:
            linenum+=1
    if not repeat and test:
        break

print("Part 2 answer:")
print(accumulator)
